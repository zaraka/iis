# encoding: utf-8

class StatesController < ApplicationController

  before_filter :request_admin
  layout 'admin'


  def create
    @state = State.new(states_params)

    respond_to do |format|
      if @state.save
        format.html { redirect_to states_path, notice: 'Země vytvořena.' }
        format.json { head :no_content }
      else
        format.html { render action: 'new' }
        format.json { render json: @state.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @state = State.find(params[:id])
    @state.destroy

    respond_to do |format|
      format.html { redirect_to states_path, notice: 'Země smazána.' }
      format.json { head :no_content }
    end

  rescue ActiveRecord::RecordNotFound
    return redirect_to states_path, alert: 'Země nenalezena.'
  end

  def new
    @state = State.new

    respond_to do |format|
      format.html {}
      format.json { head :no_content }
    end
  end

  def edit
    @state = State.find(params[:id])

    respond_to do |format|
      format.html {}
      format.json { head :no_content }
    end
  rescue ActiveRecord::RecordNotFound
    return redirect_to states_path, alert: 'Země nenalezena.'
  end

  def index
    @states = State.all

    respond_to do |format|
      format.html {}
      format.json { head :no_content }
    end
  end

  def show
    @state = State.find(params[:id])

    respond_to do |format|
      format.html {}
      format.json { head :no_content }
    end
  rescue ActiveRecord::RecordNotFound
    return redirect_to root_path, alert: 'Země nenalezena.'
  end

  def update
    @state = State.find(params[:id])

    respond_to do |format|
      if @state.update_attributes(states_params)
        format.html { redirect_to states_path, notice: 'Země bylo upravena.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @state.errors, status: :unprocessable_entity }
      end
    end
  end

  protected
  def states_params
    params.require(:state).permit(:name)
  end


end
