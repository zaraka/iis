# encoding: utf-8

class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  helper_method :current_user_session, :current_user

  #before_filter user need to be logged in
  def request_user
    if current_user
      true
    else
      redirect_to login_path
    end
  end

  #before_filter user needs to be an admin
  def request_admin
    if current_user
      if current_user.is_admin?
        return true
      else
        redirect_to root_path
      end
    else
      redirect_to login_path
    end
  end

  #before_filter user needs to be unlogged to system
  def request_unlogged
    if current_user
      redirect_to root_path
    else
      true
    end
  end


  private
  def current_user_session
    return @current_user_session if defined?(@current_user_session)
    @current_user_session = UserSession.find
  end

  def current_user
    return @current_user if defined?(@current_user)
    @current_user = current_user_session && current_user_session.user
  end

  def store_location
    session[:return_to] = request.fullpath
  end

  def redirect_back_or_default(default)
    redirect_to(session[:return_to] || default)
    session[:return_to] = nil
  end
end
