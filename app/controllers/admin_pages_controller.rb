# encoding: utf-8

class AdminPagesController < ApplicationController

  before_filter :request_admin
  layout 'admin'

  def panel
    respond_to do |format|
      format.html
    end
  end
end
