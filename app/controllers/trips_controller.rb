# encoding: utf-8

class TripsController < ApplicationController

  before_filter :request_admin
  layout 'admin'

  def create
    @trip = Trip.new(trip_params)

    respond_to do |format|
      if @trip.save
        format.html { redirect_to trips_path, notice: 'Let vytvořen.' }
        format.json { head :no_content }
      else
        format.html { render action: 'new' }
        format.json { render json: @trip.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @trip = Trip.find(params[:id])
    @trip.destroy

    respond_to do |format|
      format.html { redirect_to trips_path, notice: 'Let smazán.' }
      format.json { head :no_content }
    end

  rescue ActiveRecord::RecordNotFound
    return redirect_to trips_path, alert: 'Let nenalezen.'
  end

  def new
    @trip = Trip.new

    respond_to do |format|
      format.html {}
      format.json { head :no_content }
    end
  end

  def edit
    @trip = Trip.find(params[:id])

    respond_to do |format|
      format.html {}
      format.json { head :no_content }
    end
  rescue ActiveRecord::RecordNotFound
    return redirect_to trips_path, alert: 'Let nenalezen.'
  end

  def index
    @trips = Trip.all

    respond_to do |format|
      format.html {}
      format.json { head :no_content }
    end
  end

  def show
    @trip = Trip.find(params[:id])

    respond_to do |format|
      format.html {}
      format.json { head :no_content }
    end
  rescue ActiveRecord::RecordNotFound
    return redirect_to root_path, alert: 'Let nenalezen.'
  end

  def update
    @trip = Trip.find(params[:id])

    respond_to do |format|
      if @trip.update_attributes(trip_params)
        format.html { redirect_to trips_path, notice: 'Let byl upraven.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @trip.errors, status: :unprocessable_entity }
      end
    end
  end

  protected
  def trip_params
    params.require(:trip).permit(:from_airport_id, :to_airport_id, :base_price, :airplane_id, :departure, :arrival)
  end
end
