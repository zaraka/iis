# encoding: utf-8

class AirportsController < ApplicationController

  before_filter :request_admin
  layout 'admin'

  def create
    @airport = Airport.new(airport_params)

    respond_to do |format|
      if @airport.save
        format.html { redirect_to airports_path, notice: 'Letiště vytvořeno.' }
        format.json { head :no_content }
      else
        format.html { render action: 'new' }
        format.json { render json: @airport.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @airport = Airport.find(params[:id])
    @airport.destroy

    respond_to do |format|
      format.html { redirect_to airports_path, notice: 'Letiště smazáno.' }
      format.json { head :no_content }
    end

  rescue ActiveRecord::RecordNotFound
    return redirect_to airports_path, alert: 'Letiště nenalezeno.'
  end

  def new
    @airport = Airport.new

    respond_to do |format|
      format.html {}
      format.json { head :no_content }
    end
  end

  def edit
    @airport = Airport.find(params[:id])

    respond_to do |format|
      format.html {}
      format.json { head :no_content }
    end
  rescue ActiveRecord::RecordNotFound
    return redirect_to airports_path, alert: 'Letiště nenalezeno.'
  end

  def index
    @airports = Airport.all

    respond_to do |format|
      format.html {}
      format.json { head :no_content }
    end
  end

  def show
    @airport = Airport.find(params[:id])

    respond_to do |format|
      format.html {}
      format.json { head :no_content }
    end
  rescue ActiveRecord::RecordNotFound
    return redirect_to root_path, alert: 'Letiště nenalezeno.'
  end

  def update
    @airport = Airport.find(params[:id])

    respond_to do |format|
      if @airport.update_attributes(airport_params)
        format.html { redirect_to airports_path, notice: 'Letiště bylo upraveno.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @airport.errors, status: :unprocessable_entity }
      end
    end
  end

  protected
  def airport_params
    params.require(:airport).permit(:name, :state_id)
  end
end
