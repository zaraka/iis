# encoding: utf-8

class TicketsController < ApplicationController
  before_filter :request_admin, except: [:create, :show]
  layout 'admin', except: [:create, :show]

  def create
    trip = Trip.find(params[:ticket][:trip_id])
    unless trip.free_tickets?
      return redirect_to tickets_path, alert: 'V letadle už nejsou volná místa'
    end

    @ticket = Ticket.new(ticket_params)

    respond_to do |format|
      if @ticket.save
        format.html { redirect_to ticket_path(@ticket), notice: 'Letenka zakoupena.' }
        format.json { head :no_content }
      else
        format.html { render action: 'error' }
      end
    end
  end

  def error
    @ticket = Ticket.new

    respond_to do |format|
      format.html
    end
  end

  def destroy
    @ticket = Ticket.find(params[:id])
    @ticket.destroy

    respond_to do |format|
      format.html { redirect_to tickets_path, notice: 'Letenka smazána.' }
      format.json { head :no_content }
    end

  rescue ActiveRecord::RecordNotFound
    return redirect_to tickets_path, alert: 'Letenka nenalezena.'
  end


  def index
    @tickets = Ticket.all

    respond_to do |format|
      format.html {}
      format.json { head :no_content }
    end
  end

  def show
    @ticket = Ticket.find(params[:id])

    respond_to do |format|
      format.html {}
      format.json { head :no_content }
    end
  rescue ActiveRecord::RecordNotFound
    return redirect_to root_path, alert: 'Letenka nenalezena.'
  end

  def update
    @ticket = Ticket.find(params[:id])

    respond_to do |format|
      if @ticket.update_attributes(ticket_params)
        format.html { redirect_to tickets_path, notice: 'Letenka byla upravena.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  protected
  def ticket_params
    params.require(:ticket).permit(:user_id, :trip_id, :class_id)
  end
end
