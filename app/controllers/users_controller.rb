# encoding: utf-8

class UsersController < ApplicationController

  before_filter :request_admin, only: [:destroy, :index]


  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to login_path, notice: 'Registrace dokončena.' }
        format.json { head :no_content }

      else
        format.html { render action: 'new' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy

    respond_to do |format|
      format.html { redirect_to users_path, notice: 'Uživatel smazán.' }
      format.json { head :no_content }
    end

  rescue ActiveRecord::RecordNotFound
    return redirect_to root_path, alert: 'Uživatel nenalezen.'
  end

  def new
    @user = User.new

    respond_to do |format|
      format.html {}
      format.json { head :no_content }
    end
  end

  def edit
    @user = User.find(params[:id])

    respond_to do |format|
      format.html {}
      format.json { head :no_content }
    end
  rescue ActiveRecord::RecordNotFound
    return redirect_to root_path, alert: 'Uživatel nenalezen.'
  end

  def index
    @users = User.all

    respond_to do |format|
      format.html { render :layout => 'admin' }
      format.json { head :no_content }
    end
  end

  def show
    @user = User.find(params[:id])
    unless current_user.is_admin? || current_user == @user
      return redirect_to root_path
    end

    respond_to do |format|
      format.html {}
      format.json { head :no_content }
    end
  rescue ActiveRecord::RecordNotFound
    return redirect_to root_path, alert: 'Uživatel nenalezen.'
  end

  def update
    @user = User.find(params[:id])
    unless current_user.is_admin? || current_user != @user
      return redirect_to root_path
    end

    respond_to do |format|
      if @user.user_attributes(user_params)
        format.html { redirect_to root_path, notice: 'Uživatel byl upraven.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  protected
  def user_params
    params.require(:user).permit(:login, :email, :email_confirmation, :password, :password_confirmation, :name)
  end
end
