# encoding: utf-8

class AirplanesController < ApplicationController

  before_filter :request_admin
  layout 'admin'

  def create
    @airplane = Airplane.new(airplane_params)


    respond_to do |format|
      if @airplane.save
        format.html { redirect_to airplanes_path, notice: 'Letiště vytvořeno.' }
        format.json { head :no_content }
      else
        format.html { render action: 'new' }
        format.json { render json: @airplane.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @airplane = Airplane.find(params[:id])
    @airplane.destroy

    respond_to do |format|
      format.html { redirect_to airplanes_path, notice: 'Let smazán.' }
      format.json { head :no_content }
    end

  rescue ActiveRecord::RecordNotFound
    return redirect_to airplanes_path, alert: 'Letadlo nenalezeno.'
  end

  def new
    @airplane = Airplane.new

    respond_to do |format|
      format.html {}
      format.json { head :no_content }
    end
  end

  def edit
    @airplane = Airplane.find(params[:id])

    respond_to do |format|
      format.html {}
      format.json { head :no_content }
    end
  rescue ActiveRecord::RecordNotFound
    return redirect_to airplanes_path, alert: 'Letadlo nenalezeno.'
  end

  def index
    @airplanes = Airplane.all

    respond_to do |format|
      format.html {}
      format.json { head :no_content }
    end
  end

  def show
    @airplane = Airplane.find(params[:id])

    respond_to do |format|
      format.html {}
      format.json { head :no_content }
    end
  rescue ActiveRecord::RecordNotFound
    return redirect_to root_path, alert: 'Letadlo nenalezeno.'
  end

  def update
    @airplane = Airplane.find(params[:id])

    respond_to do |format|
      if @airplane.update_attributes(airplane_params)
        format.html { redirect_to airplanes_path, notice: 'Let byl upraven.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @airplane.errors, status: :unprocessable_entity }
      end
    end
  end

  protected
  def airplane_params
    params.require(:airplane).permit(:name, airplane_classes_attributes: [:id, :name, :plus_price, :seats, :_destroy])
  end
end
