# encoding: utf-8

class StaticPagesController < ApplicationController
  def contacts
    respond_to do |format|
      format.html
    end
  end

  def faq
    respond_to do |format|
      format.html
    end
  end

  def terms
    respond_to do |format|
      format.html
    end
  end
end
