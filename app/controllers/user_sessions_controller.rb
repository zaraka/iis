# encoding: utf-8

class UserSessionsController < ApplicationController

  before_filter :request_user, only: [:destroy]
  before_filter :request_unlogged, except: [:destroy]


  # GET /user_sessions/new
  # GET /user_sessions/new.json
  def new
    @user_session = UserSession.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user_session }
    end
  end

  # POST /user_sessions
  # POST /user_sessions.json
  def create
    @user_session = UserSession.new(login_parameters)

    #check for bans
    @user = User.find_by(:login => params[:user_session][:login])

    if @user.nil?
      return redirect_to login_path, alert: 'Uživatel nebyl nazelen.'
    end

    respond_to do |format|
      if @user_session.save
        format.html { redirect_to root_path, notice: 'Uživatel přihlášen.' }
        format.json { render json: @user_session, status: :created, location: @user_session }
      else
        format.html { render action: 'new' }
        format.json { render json: @user_session.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /user_sessions/1
  # DELETE /user_sessions/1.json
  def destroy
    @user_session = UserSession.find(params[:id])
    @user_session.destroy

    respond_to do |format|
      format.html { redirect_to login_path, notice: 'Byli jste odhlášeni' }
      format.json { head :no_content }
    end
  end

  def password_reset_new
    respond_to do |format|
      format.html
    end
  end

  def password_reset_create
    @user = User.find_by_email(params[:user_session][:email])

    if @user
      @user.create_new_password!
      redirect_to login_path, notice: "Na adresu #{params[:user_session][:email]} byly zaslány instrukce na resetování hesla."
    else
      redirect_to login_path, alert: "Email #{params[:user_session][:email]} nebyl nalezen v databázi."
    end
  end

  protected
  def login_parameters
    params.require(:user_session).permit(:login, :password)
  end
end
