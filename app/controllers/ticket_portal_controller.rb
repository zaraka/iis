class TicketPortalController < ApplicationController

  before_filter :request_user, only: [:my_tickets]
  def portal
    @trips = Trip.order('created_at DESC').limit(20)

    respond_to do |format|
      format.html
    end
  end

  def search
    @trips = Trip.where('from_airport_id = ? AND to_airport_id = ? AND departure > ?',
                        params[:search][:from_airport_id], params[:search][:to_airport_id], params[:departure].to_time.to_s(:db))

    respond_to do |format|
      format.html
    end
  end

  def my_tickets
    @tickets = current_user.tickets

    respond_to do |format|
      format.html
      format.xml
    end
  end
end
