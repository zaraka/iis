class AirplaneClass < ActiveRecord::Base

  belongs_to :airplane
  has_many :tickets

  def get_title
    "#{self.name} / +#{self.plus_price} Kč"
  end

  validates_presence_of :name, :plus_price, :seats
end
