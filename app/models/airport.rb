class Airport < ActiveRecord::Base

  belongs_to :state

  has_many :departures, :class_name => 'Trip', :foreign_key => :from_airport_id
  has_many :arrivals, :class_name => 'Trip', :foreign_key => :to_airport_id

  def count_trips
     departures.count + arrivals.count
  end

  validates_presence_of :name, :state_id
end
