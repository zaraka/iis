class Ticket < ActiveRecord::Base

  belongs_to :user
  belongs_to :airplane_class, :class_name => 'AirplaneClass', :foreign_key => :class_id
  belongs_to :trip

  def count_price
    self.trip.base_price + self.airplane_class.plus_price
  end

  validates_presence_of :trip_id, :class_id, :user_id
end
