class Trip < ActiveRecord::Base

  belongs_to :from, :class_name => 'Airport', :foreign_key => :from_airport_id
  belongs_to :to, :class_name => 'Airport', :foreign_key => :to_airport_id
  belongs_to :user
  has_many :tickets

  belongs_to :airplane

  def free_tickets?
    if self.tickets.count < self.airplane.count_seats
      true
    else
      false
    end
  end

  def free_tickets
    self.airplane.count_seats - self.tickets.count
  end

  def duration
    Time.diff(Time.parse(self.arrival.to_s), Time.parse(self.departure.to_s), '%h:%m')[:diff]
  end


  validates_presence_of :from_airport_id, :to_airport_id, :base_price, :airplane_id, :departure, :arrival, :base_price
end
