class User < ActiveRecord::Base

  acts_as_authentic do |auth|
  end

  has_many :trips
  has_many :tickets


  def is_admin?
    if self.level == 'admin'
      true
    else
      false
    end
  end

  def is_online?
    if self.last_request_at.nil?
      return false
    else
      if self.last_request_at > 15.minutes.ago
        return true
      else
        return false
      end
    end
  end

  validates_presence_of :email, :on => :create
  validates_presence_of :password_confirmation, :on => :create
  validates_presence_of :email_confirmation, :on => :create
  validates :email, :confirmation => true, :on => :create
  validates :password, :confirmation => true, :on => :create

  validates :email, :uniqueness => true
  validates :login, :uniqueness => true
  validates :login, :length => {:minimum => 4, :maximum => 16}
  validates :password, :length => {:minimum => 6}, :on => :create
end
