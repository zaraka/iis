class Airplane < ActiveRecord::Base

  has_many :trips
  has_many :airplane_classes, :inverse_of => :airplane

  def count_seats
    seats = 0
    self.airplane_classes.each do |c|
      seats += c.seats
    end
    seats
  end

  validates_presence_of :name
  validates_uniqueness_of :name
  accepts_nested_attributes_for :airplane_classes, :reject_if => lambda { |a| a[:name].blank? }, :allow_destroy => true
end
