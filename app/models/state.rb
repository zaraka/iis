class State < ActiveRecord::Base

  has_many :airports

  validates_presence_of :name
end
