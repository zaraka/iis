class CreateTrips < ActiveRecord::Migration
  def change
    create_table :trips do |t|
      t.integer :from_airplane_id
      t.integer :to_airplane_id
      t.integer :base_price
      t.integer :user_id

      t.timestamps
    end
  end
end
