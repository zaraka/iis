class CreateAirports < ActiveRecord::Migration
  def change
    create_table :airports do |t|
      t.string :name
      t.integer :state_id

      t.timestamps
    end
  end
end
