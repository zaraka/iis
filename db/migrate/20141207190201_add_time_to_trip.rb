class AddTimeToTrip < ActiveRecord::Migration
  def change
    add_column :trips, :departure, :datetime
    add_column :trips, :arrival, :datetime
  end
end
