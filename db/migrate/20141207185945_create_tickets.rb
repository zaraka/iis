class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.integer :user_id
      t.integer :class_id
      t.integer :trip_id
      t.boolean :paid, :default => false

      t.timestamps
    end
  end
end
