class RenameTripsColumns < ActiveRecord::Migration
  def change
    rename_column :trips, :from_airplane_id, :from_airport_id
    rename_column :trips, :to_airplane_id, :to_airport_id
  end
end
