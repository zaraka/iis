class CreateAirplaneClasses < ActiveRecord::Migration
  def change
    create_table :airplane_classes do |t|
      t.string :name
      t.integer :seats
      t.integer :airplane_id
      t.integer :plus_price

      t.timestamps
    end
  end
end
