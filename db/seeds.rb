# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

admin = User.create([{:login => 'admin', :email => 'neco@neco.cz', :email_confirmation => 'neco@neco.cz',
                      :password => 'admin1234', :password_confirmation => 'admin1234', :level => 'admin'}])

states = State.create([{:name => 'Česká Republika'}, {:name => 'Slovenská Republika'}, {:name => 'Polsko'}])

airports = Airport.create([{:name => 'Praha', :state_id => 1}, {:name => 'Bratislava', :state_id => 2}])