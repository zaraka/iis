# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141208181711) do

  create_table "airplane_classes", force: true do |t|
    t.string   "name"
    t.integer  "seats"
    t.integer  "airplane_id"
    t.integer  "plus_price"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "airplanes", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "airplanes_trips", force: true do |t|
    t.integer "airplane_id"
    t.integer "trip_id"
  end

  create_table "airports", force: true do |t|
    t.string   "name"
    t.integer  "state_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "states", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tickets", force: true do |t|
    t.integer  "user_id"
    t.integer  "class_id"
    t.integer  "trip_id"
    t.boolean  "paid",       default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trips", force: true do |t|
    t.integer  "from_airport_id"
    t.integer  "to_airport_id"
    t.integer  "base_price"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "departure"
    t.datetime "arrival"
    t.integer  "airplane_id"
  end

  create_table "users", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "login"
    t.string   "crypted_password",                                                                   null: false
    t.string   "password_salt",                                                                      null: false
    t.string   "persistence_token",                                                                  null: false
    t.string   "perishable_token",                                                                   null: false
    t.string   "single_access_token",                                                                null: false
    t.integer  "login_count",                                                           default: 0,  null: false
    t.integer  "failed_login_count",                                                    default: 0,  null: false
    t.datetime "last_request_at"
    t.datetime "current_login_at"
    t.datetime "last_login_at"
    t.string   "current_login_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "last_login_ip"
    t.string   "#<ActiveRecord::ConnectionAdapters::TableDefinition:0x00000005984a40>"
    t.string   "level",                                                                 default: ""
  end

end
